Rails.application.routes.draw do
  root to: "profiles#index"
  resources :profiles do
    resources :certifications
    resources :friends
    resources :hobbies
  end

  get 'info', to: 'info#index'
  get 'friendprofile', to: 'friendprofile#index'


end
