class CreateHobbies < ActiveRecord::Migration[5.2]
  def change
    create_table :hobbies do |t|
      t.string :name, null: false
      t.string :description
      t.string :preference, null: false
      t.integer :category, null: false
      t.references :profile, foreign_key: true, null: false
      t.timestamps
    end
  end
end
