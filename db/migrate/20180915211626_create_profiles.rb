class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :name, null: false
      t.string :lastname, null: false
      t.string :subtitle
      t.string :email, null: false, unique: true
      t.string :username, null: false, unique: true
      t.string :biography
      t.date :birthdate
    end
  end
end
