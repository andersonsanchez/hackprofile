class CreateFriends < ActiveRecord::Migration[5.2]
  def change
    create_table :friends do |t|
      t.string :alias, null: false
      t.string :url, null: false
      t.references :profile, foreign_key: true, null: false
      t.timestamps
    end
  end
end
