class Certification < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true
  belongs_to :profile
  has_one_attached :file
end
