class Profile < ApplicationRecord
  validates :name, presence: true
  validates :lastname, presence: true
  validates :username, presence: true
  validates :email, presence: true
  validates :email, uniqueness: true
  validates :username, uniqueness: true
  validates_associated :hobbies
  validates_associated :certifications
  validates_associated :friends
  has_one_attached :image
  has_many :hobbies
  has_many :certifications
  has_many :friends
end
