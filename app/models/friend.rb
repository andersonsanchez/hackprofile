class Friend < ApplicationRecord
  belongs_to :profile
  validates :alias, presence: true
  validates :url, presence: true
  validates :alias, uniqueness: true
  validates :url, uniqueness: true
end
