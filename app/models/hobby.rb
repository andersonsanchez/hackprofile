class Hobby < ApplicationRecord
  validates :preference, presence: true
  validates :category, presence: true
  validates :preference, numericality: { only_integer: true }
  validates :name, presence: true
  validates :name, uniqueness: true
  belongs_to :profile
  enum category: [:Música, :Lectoescritura, :Aventura, :Fitness, :Coleccionismo, :Entretenimiento, :Construcción, :Gastronomía]
end
