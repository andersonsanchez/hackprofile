class FriendsController < ApplicationController
  before_action :set_friend, only: [:edit, :show, :update, :destroy]
  before_action :set_profile, only: [:new, :create, :update, :destroy]

  def index
  end

  def new
    @friend = @profile.friends.build
  end

  def create
    @friend = Friend.new(friend_params)
    #@friend.profile_id = params[:@profile.id]
    @friend = @profile.friends.build(friend_params)
    @friend.save
    redirect_to edit_profile_path(@profile)
  end

  def show
  end

  def edit

  end

  def update
    @friend.update(friend_params)
    redirect_to edit_profile_path(@profile)
  end

  def destroy
    @friend.destroy
      redirect_to edit_profile_path(@profile)
  end

  private

  def friend_params
  params.require(:friend).permit(:alias, :url)
  end

  def set_friend
    @friend = Friend.find(params[:id])
  end

  def set_profile
    @profile = Profile.find_by(params[:profile_id])
  end
end
