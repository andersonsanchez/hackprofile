class ProfilesController < ApplicationController
	before_action :set_profile, only: [:edit, :update, :destroy]

	def new
		@profile = Profile.new
	end

	def create
		@profile = Profile.new(profile_params)
		if @profile.save
			@profile.image.attach(io: File.open(Rails.root.join("app", "assets", "images", "default.png")), filename: 'default.png' , content_type: "image/png")
			redirect_to info_path(@profile), notice: "Se ha creado tu perfil!"
		else
			redirect_to new_profile_path
		end
	end

def index
	if !params[:url]
		@profile = Profile.first
		if !@profile.nil?
			@profile = {'profile' => {'firstname' => @profile.name, 'lastname' => @profile.lastname, 'birthdate' => @profile.birthdate, 'username' => @profile.username, 'subtitle' => @profile.subtitle, 'email' => @profile.email, 'bio' => @profile.biography, 'hobbies' => @profile.hobbies, 'certificates' => @profile.certifications, 'friends' => @profile.friends, 'image' => @profile.image, 'id' => @profile.id}}
		else
			redirect_to new_profile_path
		end
	else
		@hash = RestClient.get("#{params[:url]}")
		@profile = JSON.parse(@hash)
		puts @profile
	end
end

	def show
		@profile = Profile.find(params[:id])
		respond_to do |format|
			if !@profile.nil?
				format.html
				format.json { render json: @profile}
			else
				format.html {redirect_to new_profile_path}
				format.json { render json: @profile.errors, status: :unprocessable_entity}
			end
		end
	end

	def edit
	end

	def update
		if @profile.update(profile_params)
			redirect_to profiles_path
		else
			render :edit
		end
	end

  private

    def profile_params
			params.require(:profile).permit(:name, :lastname, :subtitle, :email, :username, :biography, :birthdate, :image)
  	end

		def set_profile
			@profile = Profile.find(params[:id])
		end
end
