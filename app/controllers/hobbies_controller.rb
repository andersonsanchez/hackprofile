class HobbiesController < ApplicationController
  before_action :set_hobby, only: [:edit, :show, :update, :destroy]
  before_action :set_profile, only: [:create, :update, :new, :destroy]
  def new
    @hobby = @profile.hobbies.build
  end

  def create
    @hobby = Hobby.new(hobby_params)
    @hobby = @profile.hobbies.build(hobby_params)
    @hobby.save
    redirect_to edit_profile_path(@profile)
  end

  def show
  end

  def edit
  end

  def update
    @hobby.update(hobby_params)
    redirect_to edit_profile_path(@profile)
  end

  def destroy
    @hobby.destroy
      redirect_to edit_profile_path(@profile)
  end

  private

  def hobby_params
    params.require(:hobby).permit(:name, :description, :preference, :category)
  end

  def set_hobby
    @hobby = Hobby.find_by(params[:id])
  end

  def set_profile
    @profile = Profile.find_by(params[:profile_id])
  end


end
