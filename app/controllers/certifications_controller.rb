class CertificationsController < ApplicationController
  before_action :set_certification, only: [:edit, :show, :update, :destroy]
  before_action :set_profile, only: [:new, :create, :update, :destroy]

  def new
    @certification = @profile.certifications.build
  end

  def create
    @certification = Certification.new(certification_params)
    @certification = @profile.certifications.build(certification_params)
    @certification.save
    redirect_to edit_profile_path(@profile.id)
  end

  def show
  end

  def edit
  end

  def update
    @certification.update(certification_params)
    redirect_to edit_profile_path(@profile)
  end

  def destroy
    @certification.destroy
      redirect_to edit_profile_path(@profile)
  end

  private

  def certification_params
    params.require(:certification).permit(:name, :file)
  end

  def set_certification
    @certification = Certification.find(params[:id])
  end

  def set_profile
    @profile = Profile.find_by(params[:profile_id])
  end

end
