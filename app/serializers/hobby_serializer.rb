class HobbySerializer < ActiveModel::Serializer
  attributes :name, :description, :category
end
