class ProfileSerializer < ActiveModel::Serializer
  attributes :firstname, :lastname, :birthdate, :username, :subtitle, :email, :bio, :hobbies, :certificates, :image
  has_many :hobbies
  has_many :certifications, root: :certificates


  def certificates
  object.certifications
  end

  def bio
  object.biography
  end

  def firstname
    object.name
  end

  def image
    rails_blob_url(object.image) if object.image.attachment
  end
end
